package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class EZ_Table_App extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("EZ Table");
        URL url = this.getClass().getResource("images/logoCool.png");
        FXMLLoader loaderPrimary = new FXMLLoader(this.getClass().getResource("tableauPeriodique.fxml"));
        primaryStage.setScene(new Scene(loaderPrimary.load()));
        primaryStage.getIcons().add(new Image(String.valueOf(url)));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

}
