package controleurs;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import main.EZ_Table_App;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class MenuAideControleur {

    @FXML
    private Label description;

    @FXML
    void afficherDescription(ActionEvent event) {
        if (event.getTarget() != null && event.getTarget() instanceof Button) {
            String target = ((Button) event.getSource()).getId();
            description.setText(recupererText(target.substring(6)));
        }
    }

    private String recupererText(String nom)
    {
        String retour = "";
        try {
            URL url = EZ_Table_App.class.getResource("aide/" + nom + ".txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8));
            retour = String.valueOf(br.readLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return retour;
    }
}
