package controleurs;

import csv.Atome;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Line;

public class SchemasControleur {

    public final static String STYLE_LEWIS = "-fx-font-size: 18";

    @FXML
    private GridPane zoneDessin;

    @FXML
    private Label label;

    private Atome atome1, atome2;

    int gauche = 0;
    int droite = 0;
    int indiceX = 0;
    int nbElectronsManquants1;
    int nbElectronsManquants2;
    private boolean possible = true;

    public void initialiser() {
        trouverLiaison();
    }

    public void setAtomes(Atome atome1, Atome atome2) {
        this.atome1 = atome1;
        this.atome2 = atome2;
    }

    public boolean isPossible() {
        return possible;
    }

    private void trouverLiaison() {
        int nbTempElectronsManquants1 = atome1.getFamille() == 1 ? 1 : 18 - atome1.getFamille();
        int nbTempElectronsManquants2 = atome2.getFamille() == 1 ? 1 : 18 - atome2.getFamille();
        if (atome1.getFamille() == 13) {
            nbTempElectronsManquants1 = 3;
        }
        if (atome2.getFamille() == 13) {
            nbTempElectronsManquants2 = 3;
        }

        if (nbTempElectronsManquants2 > nbTempElectronsManquants1 && !atome1.getAbreviation().equals("H") && !atome2.getAbreviation().equals("H")
        || atome1.getAbreviation().equals("H")) {
            Atome temp = new Atome(atome2);
            atome2 = new Atome(atome1);
            atome1 = new Atome(temp);
        }

        // Exceptions
        if (atome1.getAbreviation().equals("N") && atome2.getAbreviation().equals("O")) {
            gererExceptionAzoteOxy();
        }
        else if (atome1.getAbreviation().equals("C") && atome2.getAbreviation().equals("C")) {
            gererExceptionCarboneDiatomique();
        }
        else if (!atome2.getAbreviation().equals("H") && (nbTempElectronsManquants1 == 4 && nbTempElectronsManquants2 == 4 || Math.abs(atome1.getElectronegativite() - atome2.getElectronegativite()) > 1.7)) {
            alertPerso();
            possible = false;
        }

        else {
            nbElectronsManquants1 = atome1.getFamille() == 1 ? 1 : 18 - atome1.getFamille();
            nbElectronsManquants2 = atome2.getFamille() == 1 ? 1 : 18 - atome2.getFamille();

            if (atome1.getFamille() == 13) {
                nbElectronsManquants1 = 3;
            }
            if (atome2.getFamille() == 13) {
                nbElectronsManquants2 = 3;
            }

            int nbAtome1 = 1, nbAtome2;
            int nbLiensAtome1 = 0;

            while (((nbElectronsManquants1 - nbLiensAtome1) * nbAtome1) % nbElectronsManquants2 != 0) {
                if (nbLiensAtome1 < 3) {
                    nbAtome1++;
                    nbLiensAtome1++;
                }
                else {

                }
            }
            nbAtome2 = ((nbElectronsManquants1 - nbLiensAtome1) * nbAtome1) / nbElectronsManquants2;


            Atome atome1Ecriture = atome1;
            Atome atome2Ecriture = atome2;
            int nbAtome1Ecriture = nbAtome1;
            int nbAtome2Ecriture = nbAtome2;
            if (atome1.getElectronegativite() > atome2.getElectronegativite() && !(atome1.getAbreviation().equals("C") && atome2.getAbreviation().equals("H") || atome1.getAbreviation().equals("N") && atome2.getAbreviation().equals("H"))) {
                atome1Ecriture = atome2;
                atome2Ecriture = atome1;
                nbAtome1Ecriture = nbAtome2;
                nbAtome2Ecriture = nbAtome1;
            }
            String retour = atome1Ecriture.getAbreviation();

            if (!atome1Ecriture.getAbreviation().equals(atome2Ecriture.getAbreviation())) {
                if (nbAtome1Ecriture > 1) {
                    retour += nbAtome1Ecriture;
                }
                retour += atome2Ecriture.getAbreviation();
                if (nbAtome2Ecriture > 1) {
                    retour += nbAtome2Ecriture;
                }
            }
            else
                retour += nbAtome1Ecriture + nbAtome2Ecriture;

            label.setText("Molécule : " + retour);

            placerAtomesCentraux(nbAtome1, nbLiensAtome1);
            placerAtomesExt(nbAtome1, nbAtome2, nbElectronsManquants2);
        }
    }

    private void placerAtomesCentraux(int nbAtome1, int nbLiensAtome1) {
        indiceX = 6;
        int nbAtome1Restants = nbAtome1;

        if (nbAtome1 % 2 == 0) {
            indiceX = 5;
        }

        Label symbole = new Label(atome1.getAbreviation());
        symbole.setStyle(STYLE_LEWIS);
        zoneDessin.add(symbole, indiceX, 2);
        nbAtome1Restants--;

        for (int i = 1; i <= nbAtome1Restants; i++) {
            Label symboleRestant = new Label(atome1.getAbreviation());
            symboleRestant.setStyle(STYLE_LEWIS);
            if (i <= nbAtome1Restants / 2) {
                gauche++;
                zoneDessin.add(symboleRestant, indiceX - gauche * 2, 2);
                placerLignesX(nbLiensAtome1, (indiceX - gauche * 2) + 1);
            }
            else {
                droite++;
                zoneDessin.add(symboleRestant, indiceX + droite * 2, 2);
                placerLignesX(nbLiensAtome1, (indiceX + droite * 2) - 1);
            }
        }
    }

    private void placerAtomesExt(int nbAtome1, int nbAtome2, int nbElectronsManquants2) {
        int i = 0;
        int nbAtome2Restants = nbAtome2;
        boolean hauteur = true;
        int dgauche =0;
        int ddroite = 0;
        while (nbAtome2Restants > 0) {
            Label symboleAtome2 = new Label(atome2.getAbreviation());
            symboleAtome2.setStyle(STYLE_LEWIS);
            i++;
            nbAtome2Restants--;

            switch (i) {
                case 1:
                    int indiceXAtomeActuelGauche = indiceX - (gauche + 1) * 2;
                    zoneDessin.add(symboleAtome2, indiceXAtomeActuelGauche, 2);
                    placerLignesX(nbElectronsManquants2, indiceXAtomeActuelGauche + 1);
                    break;
                case 2:
                    int indiceXAtomeActuelDroite = indiceX + (droite + 1) * 2;
                    zoneDessin.add(symboleAtome2, indiceXAtomeActuelDroite, 2);
                    placerLignesX(nbElectronsManquants2, indiceXAtomeActuelDroite - 1);
                    break;

            }

            //haut-bas à gauche
            if (i > 2 && i % 2 == 0) {
                if (hauteur) {
                        zoneDessin.add(symboleAtome2, indiceX - (dgauche) * 2, 4);
                        placerLignesY(nbElectronsManquants2, indiceX - (dgauche) * 2, 3);
                        hauteur = false;
                        dgauche++;
                    }
                 else {
                     if(nbAtome1 >1) {
                         zoneDessin.add(symboleAtome2, indiceX - (dgauche- 1) * 2, 0);
                         placerLignesY(nbElectronsManquants2, indiceX - (dgauche- 1) * 2, 1);
                     }
                     else {
                         zoneDessin.add(symboleAtome2, indiceX - dgauche * 2, 0);
                         placerLignesY(nbElectronsManquants2, indiceX - dgauche * 2, 1);
                     }
                    hauteur = true;
                    dgauche++;
                }
            }

            //haut-bas à droite
            if (i > 1 && i % 2 == 1) {
                if (hauteur) {
                    zoneDessin.add(symboleAtome2, indiceX + ddroite * 2, 4);
                    placerLignesY(nbElectronsManquants2, indiceX + ddroite * 2, 3);
                    hauteur = false;
                    ddroite++;
                }
                else {

                    if(nbAtome1 > 1) {
                        zoneDessin.add(symboleAtome2, indiceX + (ddroite+1) * 2, 0);
                        placerLignesY(nbElectronsManquants2, indiceX + (ddroite+1) * 2, 1);
                    }
                    else {
                        zoneDessin.add(symboleAtome2, indiceX + ddroite * 2, 0);
                        placerLignesY(nbElectronsManquants2, indiceX + ddroite * 2, 1);
                    }
                    hauteur = true;
                    ddroite++;
                }
            }
        }
    }

    private void placerLignesX(int nbLiens, int indiceX) {
        for (int j = 0; j < nbLiens; j++) {
            Line ligne = new Line();
            ligne.setEndX(37);

            if (nbLiens == 2) {
                switch (j) {
                    case 0:
                        ligne.setTranslateY(5);
                        break;
                    case 1:
                        ligne.setTranslateY(-5);
                }
            }
            else {
                switch (j) {
                    case 1:
                        ligne.setTranslateY(10);
                        break;
                    case 2:
                        ligne.setTranslateY(-10);
                }
            }

            zoneDessin.add(ligne, indiceX, 2);
        }
    }

    private void placerLignesY(int nbLiens, int indiceX, int indiceY) {
        for (int j = 0; j < nbLiens; j++) {
            Line ligne = new Line();
            ligne.setEndY(37);

            if (nbLiens == 2) {
                switch (j) {
                    case 0:
                        ligne.setTranslateX(5);
                        break;
                    case 1:
                        ligne.setTranslateX(-5);
                }
            } else {
                switch (j) {
                    case 1:
                        ligne.setTranslateX(10);
                        break;
                    case 2:
                        ligne.setTranslateX(-10);
                }
            }

            zoneDessin.add(ligne, indiceX, indiceY);
        }
    }

    private void gererExceptionAzoteOxy() {
        label.setText("Molécule : N2O");
        indiceX = 3;

        for (int i = 0; i < 2; i++) {
            indiceX += 2;
            Label n = new Label("N");
            n.setStyle(STYLE_LEWIS);
            zoneDessin.add(n, indiceX, 2);
        }

        Label o = new Label("O");
        o.setStyle(STYLE_LEWIS);
        zoneDessin.add(o, indiceX, 0);

        placerLignesX(3, 6);
        placerLignesY(1, indiceX, 1);
    }

    private void gererExceptionCarboneDiatomique() {
        label.setText("Molécule : C2");
        indiceX = 3;

        for(int i = 0; i < 2; i++) {
            indiceX += 2;
            Label c = new Label("C");
            c.setStyle(STYLE_LEWIS);
            zoneDessin.add(c, indiceX, 2);
        }

        placerLignesX(2, 6);
    }

    private void alertPerso() {
        Alert alerte = new Alert(Alert.AlertType.ERROR);
        alerte.setContentText("Aucune liaison covalente n'est possible entre les deux atomes sélectionnés");
        alerte.setHeaderText("Erreur!");
        alerte.showAndWait();
    }
}