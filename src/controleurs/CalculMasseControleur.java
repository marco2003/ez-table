package controleurs;

import csv.Atome;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class CalculMasseControleur {

    @FXML
    private TextField textFieldMolecule;

    @FXML
    private Label masseMolecule;
    private Stage fenetreCalculateur;

    private HashMap<String, Atome> atomes;

    void initialiser() {
        fenetreCalculateur.getScene().setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER))
                calcul();
        });
    }

    private void calcul() {
        String temp = "";
        double masse1 = 0;
        double masse2 = 0;
        double masse3 = 0;
        double masseTotale = 0;
        String temp1;
        String temp2;
        String temp3;

        if (!textFieldMolecule.getText().isEmpty()) {
            temp = textFieldMolecule.getText() + " ";
            if (temp.contains("(") && temp.contains(")")) {
                if (temp.charAt(0) != '(') {
                    temp1 = temp.substring(0, temp.indexOf('(')) + " ";
                    masse1 = calculer(temp1);
                }
                temp2 = temp.substring(temp.indexOf('(') + 1, temp.indexOf(')')) + " ";

                if (Character.isDigit(temp.charAt(temp.indexOf(')') + 1))) {
                    masse2 = calculer(temp2) * Double.parseDouble(String.valueOf(temp.charAt(temp.indexOf(')') + 1)));
                    try {
                        temp3 = temp.substring(temp.indexOf(')') + 2) + " ";
                        masse3 = calculer(temp3);
                    } catch (StringIndexOutOfBoundsException e) {
                        System.out.println(("out of bound"));
                    }
                } else {
                    masse2 = calculer(temp2);
                    temp3 = temp.substring(temp.indexOf(')') + 1) + " ";
                    masse3 = calculer(temp3);
                }
                masseTotale = masse1 + masse2 + masse3;

            } else {
                masseTotale = calculer(temp);
            }
        } else {
            Alert alerte = new Alert(Alert.AlertType.ERROR);
            alerte.setContentText("La zone de texte est vide!");
            alerte.setHeaderText("Erreur!");
            alerte.showAndWait();
        }
        if (masseTotale == 0 && !textFieldMolecule.getText().isEmpty()) {
            alertPerso();
        } else {
            masseTotale = ((double) (Math.round(masseTotale * 100))) / 100;
        }
        masseMolecule.setText(masseTotale + " g/mol");
    }

    @FXML
    void calculerMasseMolaire(ActionEvent event) {
        calcul();
    }

    private double calculer(String st) {
        double masseMolaire = 0;
        boolean trouve;
        Set<String> symbAtomes = atomes.keySet();

        char[] arrayChar = st.toCharArray();
        for (int i = 0; i < arrayChar.length; i++) {
            char temp = arrayChar[i];
            if (temp != ' ') {
                trouve = false;
                if (!Character.isDigit(arrayChar[i + 1])) {
                    if (!Character.isLowerCase(arrayChar[i + 1])) {
                        Iterator<String> iterAtomes = symbAtomes.iterator();

                        while (iterAtomes.hasNext() && !trouve) {
                            String symbAtomeActuel = iterAtomes.next();
                            if (Character.toString(temp).equals(symbAtomeActuel)) {
                                masseMolaire += atomes.get(symbAtomeActuel).getMasse();
                                trouve = true;
                            }
                        }

                    } else {
                        String tempo = Character.toString(arrayChar[i]) + arrayChar[i + 1];
                        Iterator<String> iterAtomes = symbAtomes.iterator();

                        while (iterAtomes.hasNext() && !trouve) {
                            String symbAtomeActuel = iterAtomes.next();

                            if (tempo.equals(symbAtomeActuel)) {
                                trouve = true;
                                if (Character.isDigit(arrayChar[i + 2])) {
                                    if (!Character.isDigit(arrayChar[i + 3])) {
                                        masseMolaire += atomes.get(symbAtomeActuel).getMasse() * Double.parseDouble(Character.toString(arrayChar[i + 2]));
                                        i += 2;
                                    } else {
                                        String temporaire = Character.toString(arrayChar[i + 2]) + arrayChar[i + 3];
                                        masseMolaire += atomes.get(symbAtomeActuel).getMasse() * Double.parseDouble(temporaire);
                                        i += 3;
                                    }
                                } else {
                                    masseMolaire += atomes.get(symbAtomeActuel).getMasse();
                                    i++;
                                }
                            }
                        }

                    }
                } else {
                    Iterator<String> iterAtomes = symbAtomes.iterator();

                    while (iterAtomes.hasNext() && !trouve) {
                        String symbAtomeActuel = iterAtomes.next();

                        if (Character.toString(temp).equals(symbAtomeActuel)) {
                            trouve = true;

                            if (!Character.isDigit(arrayChar[i + 2])) {
                                masseMolaire += atomes.get(symbAtomeActuel).getMasse() * Double.parseDouble(Character.toString(arrayChar[i + 1]));
                                i++;
                            } else {
                                String temporaire = Character.toString(arrayChar[i + 1]) + arrayChar[i + 2];
                                masseMolaire += atomes.get(symbAtomeActuel).getMasse() * Double.parseDouble(temporaire);
                                i += 2;
                            }
                        }
                    }
                }
                if (trouve == false) {
                    i = arrayChar.length;
                    masseMolaire = 0.0;
                }
            }
        }
        return masseMolaire;
    }


    private void alertPerso() {
        Alert alerte = new Alert(Alert.AlertType.ERROR);
        alerte.setContentText("Le ou les atomes saisi(s) n'existent pas!");
        alerte.setHeaderText("Erreur!");
        alerte.showAndWait();
    }

    @FXML
    void retourFenetrePrincipale(MouseEvent event) {
        fenetreCalculateur.close();
    }

    public void setFenetreCalculateur(Stage fenetreCalculateur) {
        this.fenetreCalculateur = fenetreCalculateur;
    }

    public void setAtomes(HashMap<String, Atome> atomes) {
        this.atomes = atomes;
    }
}
