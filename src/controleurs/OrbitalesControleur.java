package controleurs;

import csv.Atome;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import main.EZ_Table_App;
import com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import java.util.ArrayList;
import java.util.HashMap;


public class OrbitalesControleur {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private GridPane grilleAtomesBase;

    @FXML
    private Label labelElectrons;


    TableauControleur tableauControleur;
    SubScene subScene;
    private Group model = new Group();
    PerspectiveCamera camera = new PerspectiveCamera(true);

    Translate pivot = new Translate();
    Rotate yRotate = new Rotate(0, Rotate.Y_AXIS);
    Rotate xRotate = new Rotate(0, Rotate.X_AXIS);

    private double ancreX, ancreY;
    private double angleAncreX = 0;
    private double angleAncreY = 0;
    private final DoubleProperty angleX = new SimpleDoubleProperty(0);
    private final DoubleProperty angleY = new SimpleDoubleProperty(0);

    @FXML
    void initialize() {

        grilleAtomesBase.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent me) -> {
            if (me.getTarget() instanceof Button) {
                if (!me.isControlDown()) {
                    model.getChildren().clear();
                }
                Button boutonClique = (Button) me.getTarget();
                String idBouton = boutonClique.getId().replace("b", "");
                lireFichiers("objets3D/" + idBouton + ".obj");
            }
        });

        //Création et position de la sous-scène
        subScene = new SubScene(model, 312, 305, true, SceneAntialiasing.DISABLED);
        subScene.setFill(Color.GRAY);
        anchorPane.getChildren().add(subScene);
        AnchorPane.setTopAnchor(subScene, 132.0);

        //Lumière
        PointLight lumiereBasse = new PointLight();
        lumiereBasse.getTransforms().add(new Translate(0, -1000, 0));
        PointLight lumiereHaute = new PointLight();
        lumiereHaute.getTransforms().add(new Translate(0, 1000, 0));
        model.getChildren().addAll(lumiereBasse, lumiereHaute);

        //Importation des objets 3D

        camera.getTransforms().addAll (
                pivot,
                yRotate,
                xRotate,
                new Translate(0, 0, -25)
        );
        initMouseControl(model, subScene);
        subScene.setCamera(camera);
    }

    private void initMouseControl(Group group, SubScene subScene) {
        Rotate xRotate;
        Rotate yRotate;
        group.getTransforms().addAll(
                xRotate = new Rotate(0, Rotate.X_AXIS),
                yRotate = new Rotate(0, Rotate.Y_AXIS)
        );
        xRotate.angleProperty().bind(angleX);
        yRotate.angleProperty().bind(angleY);

        subScene.setOnMousePressed(event -> {
            ancreX = event.getSceneX();
            ancreY = event.getSceneY();
            angleAncreX = angleX.get();
            angleAncreY = angleY.get();
        });

        subScene.setOnMouseDragged(event -> {
            angleX.set(angleAncreX - (ancreY - event.getSceneY()));
            angleY.set(angleAncreY + ancreX - event.getSceneX());
        });

        subScene.setOnScroll(event -> {
            if (event.getDeltaY() > 0) {
                model.setScaleX(model.getScaleX()*1.25);
                model.setScaleY(model.getScaleY()*1.25);
                model.setScaleZ(model.getScaleZ()*1.25);
            }
            else {
                model.setScaleX(model.getScaleX()*0.80);
                model.setScaleY(model.getScaleY()*0.80);
                model.setScaleZ(model.getScaleZ()*0.80);
            }
        });
    }

    private void lireFichiers(String... urls) {
        ObjModelImporter objModelImporter = new ObjModelImporter();

        for (String urlActuel : urls) {
            objModelImporter.read(EZ_Table_App.class.getResource(urlActuel));
            for (MeshView mv : objModelImporter.getImport()) {
                model.getChildren().add(mv);
            }
            objModelImporter.clear();
        }
    }

    public void setTableauControleur(TableauControleur tableauControleur) {
        this.tableauControleur = tableauControleur;
    }

    private void setCouleur(Atome atome) {
        HashMap<String, Button> listeBouton = new HashMap<>();
        for (Node b : grilleAtomesBase.getChildren()) {
            if (b instanceof Button)
                listeBouton.put(b.getId(), (Button) b);
        }
        ArrayList<String> listeOrdonneeString = atome.getTabConfigElectronique();
        ArrayList<Button> listeOrdonneeBoutons = new ArrayList<>();

        for (String s : listeOrdonneeString) {
            listeOrdonneeBoutons.add(listeBouton.get("b" + s));
        }

        String[] configElectronique = atome.developperGazNoble(atome.getConfigElectroniqueText()).split(" ");
        labelElectrons.setText(atome.getNom() + " : " + atome.getConfigElectroniqueText());
        int indiceBouton = 0;

        for(int i = 0 ; i < configElectronique.length; i++)
        {
            String couche = configElectronique[i];
            int maxElectrons;
            if (couche.contains("s"))
                maxElectrons = 2;
            else if (couche.contains("p"))
                maxElectrons = 6;
            else if (couche.contains("d"))
                maxElectrons = 10;
            else
                maxElectrons = 14;

            int nbElectronsAPlacer = Integer.parseInt(couche.substring(2));
            int nbPlacesRestantes = maxElectrons;
            while (nbElectronsAPlacer > nbPlacesRestantes/2) {
                listeOrdonneeBoutons.get(indiceBouton).setStyle("-fx-background-color: lightgreen");
                nbPlacesRestantes -= 2;
                nbElectronsAPlacer -= 2;
                indiceBouton++;
            }
            while (nbElectronsAPlacer > 0) {
                listeOrdonneeBoutons.get(indiceBouton).setStyle("-fx-background-color: lightyellow");
                nbElectronsAPlacer -= 1;
                indiceBouton++;
            }
        }
    }
    @FXML
    void regenerer(ActionEvent event) {
        initialiserAtome();
    }

    public void initialiserAtome() {
        Button boutonActuel = tableauControleur.getBoutonSelectionne();
        model.getChildren().clear();
        if (boutonActuel != null) {
            Atome atome = (Atome) tableauControleur.getBoutonSelectionne().getUserData();
            if (atome != null) {
                setCouleur(atome);

                ArrayList<String> config = atome.getTabConfigElectronique();
                String[] orbitales = new String[config.size()];

                for (int i = 0; i < config.size(); i++) {
                    orbitales[i] = "objets3D/" + config.get(i) + ".obj";
                }
                lireFichiers(orbitales);
            }
        }
    }
}
